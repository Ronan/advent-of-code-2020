package com.leroy.ronan.aoc2020.day16;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Ticket {

    private List<Long> values;

    public Ticket(long...values) {
        this.values = Arrays.stream(values)
                .boxed()
                .collect(Collectors.toList());
    }

    public Ticket(String input) {
        this.values = Arrays.stream(input.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }

    public long getErrorRate(List<Rule> rules) {
        var res = 0L;
        for (var value : values) {
            var valid = false;
            for (var rule : rules) {
                if (rule.accept(value)) {
                    valid = true;
                    break;
                }
            }
            if (!valid) {
                res += value;
            }
        }
        return res;
    }

    public int size() {
        return this.values.size();
    }

    public Long get(int i) {
        return values.get(i);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(values, ticket.values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(values);
    }
}
