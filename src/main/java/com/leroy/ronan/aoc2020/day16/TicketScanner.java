package com.leroy.ronan.aoc2020.day16;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TicketScanner {

    private final List<String> input;
    private List<Rule> rules = new ArrayList<>();
    private Ticket myTicket;
    private List<Ticket> nearbyTickets = new ArrayList<>();
    private List<Ticket> validTickets;
    private Map<String, List<Integer>> matches = new HashMap<>();
    private Map<String, Integer> indexOfFields = new HashMap<>();

    public TicketScanner(String source) throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        for (var current : input) {
            if ("".equals(current)) {
                break;
            }
            rules.add(new Rule(current));
        }

        this.myTicket = new Ticket(input.get(input.indexOf("your ticket:") + 1));

        var index = input.indexOf("nearby tickets:");
        for (var i = index+1; i < input.size(); i++) {
            nearbyTickets.add(new Ticket(input.get(i)));
        }

        this.validTickets = this.nearbyTickets.stream()
                .filter(t -> t.getErrorRate(this.rules) == 0)
                .collect(Collectors.toList());

        for (var rule : rules) {
            matches.put(rule.getName(), new ArrayList<>());
            for (int i = 0; i < myTicket.size(); i++) {
                var allMatch = true;
                for (var ticket : validTickets) {
                    if (!rule.accept(ticket.get(i))) {
                        allMatch = false;
                        break;
                    }
                }
                if (allMatch) {
                    matches.get(rule.getName()).add(i);
                }
            }
        }

        while (matches.size() > 0) {
            List<String> resolved = new ArrayList<>();
            for (var entry : matches.entrySet()) {
                if (entry.getValue().size() == 1) {
                    indexOfFields.put(entry.getKey(), entry.getValue().get(0));
                    resolved.add(entry.getKey());
                }
            }
            if (resolved.size() == 0) {
                throw new RuntimeException("problem is not resolvable");
            }
            for (var current : resolved) {
                matches.remove(current);
                for (var entry : matches.entrySet()) {
                    entry.getValue().remove(indexOfFields.get(current));
                }
            }
        }
    }

    public long getErrorRate() {
        var res = 0L;
        for (var ticket : nearbyTickets) {
            res += ticket.getErrorRate(this.rules);
        }
        return res;
    }

    public List<Rule> getRules() {
        return this.rules;
    }

    public Ticket getMyTicket() {
        return this.myTicket;
    }

    public List<Ticket> getNearbyTickets() {
        return this.nearbyTickets;
    }

    public long getMyTicket(String name) {
        return myTicket.get(indexOfFields.get(name));
    }

    public long getProductOf(String start) {
        var values = rules.stream()
                .map(Rule::getName)
                .filter(s -> s.startsWith(start))
                .mapToLong(this::getMyTicket)
                .boxed()
                .collect(Collectors.toList());
        long res = 1;
        for (var value : values) {
            res = res * value;
        }
        return res;
    }
}
