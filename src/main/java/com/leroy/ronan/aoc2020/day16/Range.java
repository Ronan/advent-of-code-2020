package com.leroy.ronan.aoc2020.day16;

public class Range {

    private String input;
    private long min;
    private long max;

    public Range(String input) {
        this.input = input;
        var splitted = input.split("-");
        this.min = Long.parseLong(splitted[0]);
        this.max = Long.parseLong(splitted[1]);
    }

    public boolean accept(Long value) {
        return min <= value && value <= max;
    }
}
