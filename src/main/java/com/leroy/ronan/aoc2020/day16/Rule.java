package com.leroy.ronan.aoc2020.day16;

import java.util.ArrayList;
import java.util.List;

public class Rule {

    private String input;
    private String name;
    private List<Range> ranges = new ArrayList<>();

    public Rule(String input) {
        this.input = input;
        var splitted = input.split(":");
        this.name = splitted[0];
        for (var current : splitted[1].split("or")) {
            this.ranges.add(new Range(current.trim()));
        }
    }

    public String getName() {
        return this.name;
    }

    public boolean accept(Long value) {
        boolean res = false;
        for (var range : ranges) {
            if (range.accept(value)) {
                res = true;
                break;
            }
        }
        return res;
    }

}
