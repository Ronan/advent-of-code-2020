package com.leroy.ronan.aoc2020.day11;

public enum Direction {
    NORTH(0, -1),
    NORTH_EAST(1, -1),
    EAST(1, 0),
    SOUTH_EAST(1 ,1),
    SOUTH(0, 1),
    SOUTH_WEST(-1, 1),
    WEST(-1, 0),
    NORTH_WEST(-1, -1);

    private final int xStep;
    private final int yStep;

    Direction(int xStep, int yStep) {
        this.xStep = xStep;
        this.yStep = yStep;
    }

    public int nextX(int x) {
        return x + xStep;
    }

    public int nextY(int y) {
        return y + yStep;
    }
}
