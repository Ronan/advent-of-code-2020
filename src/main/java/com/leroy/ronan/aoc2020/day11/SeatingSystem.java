package com.leroy.ronan.aoc2020.day11;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class SeatingSystem {

    private List<String> input;
    private char[][] grid;
    private BiFunction<Integer, Integer, Boolean> isAttractive = this::isAttractive;
    private BiFunction<Integer, Integer, Boolean> isRepulsive = this::isRepulsive;

    public SeatingSystem(String source) throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        grid = new char[input.size()][];
        for (var y = 0; y < input.size(); y++) {
            grid[y] = input.get(y).toCharArray();
        }
        System.out.println(this.toString());
    }

    public void setIsAttractive(BiFunction<Integer, Integer, Boolean> isAttractive) {
        this.isAttractive = isAttractive;
    }

    public void setIsRepulsive(BiFunction<Integer, Integer, Boolean> isRepulsive) {
        this.isRepulsive = isRepulsive;
    }

    public void step() {
        char[][] newgrid = new char[getLineCount()][getLineSize()];
        for (var y = 0; y < getLineCount(); y++) {
            for (var x = 0; x < getLineSize(); x++) {
                switch(getStatus(x, y)) {
                    case 'L':
                        if (isAttractive.apply(x, y)) {
                            newgrid[y][x] = '#';
                        } else {
                            newgrid[y][x] = 'L';
                        }
                        break;
                    case '#':
                        if (isRepulsive.apply(x, y)) {
                            newgrid[y][x] = 'L';
                        } else {
                            newgrid[y][x] = '#';
                        }
                        break;
                    case '.':
                        newgrid[y][x] = '.';
                        break;
                }
            }
        }
        grid = newgrid;
        System.out.println(this.toString());
    }

    private boolean isRepulsive(int x, int y) {
        return getNextOccupiedSeatsCount(x, y) >= 4;
    }

    private boolean isAttractive(int x, int y) {
        return getNextOccupiedSeatsCount(x, y) == 0;
    }

    public void stabilize() {
        var previous = this.toString();
        step();
        while (!previous.equals(this.toString())) {
            previous = this.toString();
            step();
        }
    }

    protected char getStatus(int x, int y) {
        var res = ' ';
        if (0 <= x && x < getLineSize() && 0 <= y && y < getLineCount())
            res = grid[y][x];
        return res;
    }

    private int getLineSize() {
        return grid[0].length;
    }

    private int getLineCount() {
        return grid.length;
    }

    private int getNextOccupiedSeatsCount(int x, int y) {
        var res = 0;
        for (var j = -1; j <= 1; j++) {
            for (var i = -1; i <= 1; i++) {
                if (i != 0 || j != 0) {
                    if (getStatus(x + i, y + j) == '#') {
                        res = res +1;
                    }
                }
            }
        }
        return res;
    }

    public int getSeenOccupiedSeatsCount(int x, int y) {
        var res = 0;
        for (var direction : Direction.values()) {
            var curX = direction.nextX(x);
            var curY = direction.nextY(y);
            var curStatus = getStatus(curX, curY);
            while (curStatus == '.') {
                curX = direction.nextX(curX);
                curY = direction.nextY(curY);
                curStatus = getStatus(curX, curY);
            }
            if (curStatus == '#') {
                res = res + 1;
            }
        }
        return res;
    }

    public long getNextOccupiedSeatsCount() {
        var res = 0L;
        for (var y = 0; y < getLineCount(); y++) {
            for (var x = 0; x < getLineSize(); x++) {
                if (getStatus(x, y) == '#') {
                    res = res +1;
                }
            }
        }
        return res;
    }

    public long getNonOccupiedSeatsCount() {
        var res = 0L;
        for (var y = 0; y < getLineCount(); y++) {
            for (var x = 0; x < getLineSize(); x++) {
                if (getStatus(x, y) == 'L') {
                    res = res +1;
                }
            }
        }
        return res;
    }

    public String toString() {
        return toString(this.grid);
    }

    public String toString(char[][] grid) {
        StringBuilder res = new StringBuilder();
        for (var y = 0; y < grid.length; y++) {
            for (var x = 0; x < grid[0].length; x++) {
                res.append(grid[y][x]);
            }
            res.append('\n');
        }
        return res.toString();
    }

}
