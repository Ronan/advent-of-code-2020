package com.leroy.ronan.aoc2020.day14;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DockingProgram {

    private final List<String> input;
    private HashMap<String, Long> memory;
    private int version = 1;

    public DockingProgram(String source) throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void run() {
        memory = new HashMap<>();
        String mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
        for (var instruction : input) {
            if (instruction.startsWith("mask")) {
                mask = instruction.split(" ")[2];
            } else if (instruction.startsWith("mem")) {
                var index = instruction.substring(instruction.indexOf('[') + 1, instruction.indexOf(']'));
                var value = instruction.split(" ")[2];
                switch (version) {
                    case 1:
                        memory.put(index, applyMask(value, mask));
                        break;
                    case 2:
                        for (var currentIndex : getMaskedIndexes(toBinary(index), mask)) {
                            memory.put(currentIndex, Long.valueOf(value));
                        }
                        break;
                }
            }
        }
    }

    protected static long applyMask(String value, String mask) {
        var binary = toBinary(value);
        var res = new StringBuilder(binary);
        for (int i = 0; i < res.length(); i++) {
            if (mask.charAt(i) != 'X') {
                res.setCharAt(i, mask.charAt(i));
            }
        }
        return toLong(res.toString());
    }

    protected static List<String> getMaskedIndexes(String address, String mask) {
        var combined = combine(address, mask);
        return decompose(combined);
    }

    private static List<String> decompose(String combined) {
        List<String> res;
        var index = combined.indexOf('X');
        if (index < 0) {
            res = List.of(combined);
        } else {
            var s0 = new StringBuilder(combined);
            s0.setCharAt(index, '0');
            var s1 = new StringBuilder(combined);
            s1.setCharAt(index, '1');

            res = Stream.concat(
                    decompose(s0.toString()).stream(),
                    decompose(s1.toString()).stream()
                ).collect(Collectors.toList());

        }
        return res;
    }

    protected static String combine(String address, String mask) {
        var res = new StringBuilder(address);
        for (int i = 0; i < res.length(); i++) {
            if (mask.charAt(i) != '0') {
                res.setCharAt(i, mask.charAt(i));
            }
        }
        return res.toString();
    }

    protected static String toBinary(String value) {
        var longValue = Long.parseLong(value);
        var res = new StringBuilder("000000000000000000000000000000000000");
        for (var i = 1; i <= res.length(); i++) {
            if (longValue % 2 != 0) {
                res.setCharAt(res.length() - i, '1');
            }
            longValue = longValue / 2;
        }
        return res.toString();
    }

    protected static long toLong(String value) {
        long res = 0;
        long weight = 1;
        for (var i = 1; i <= value.length(); i++) {
            if (value.charAt(value.length() - i) == '1') {
                res = res + weight;
            }
            weight = weight * 2;
        }
        return res;
    }

    public long getMemorySum() {
        return memory.values().stream().mapToLong(l -> l).sum();
    }

}
