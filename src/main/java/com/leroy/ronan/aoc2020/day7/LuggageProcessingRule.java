package com.leroy.ronan.aoc2020.day7;

import java.util.HashMap;
import java.util.Map;

public class LuggageProcessingRule {

    private String rule;
    private String container;
    private Map<String, Long> content;

    public LuggageProcessingRule(String rule) {
        this.rule = rule;
        container = rule.substring(0, rule.indexOf("bags contain")).trim();
        content = new HashMap<>();
        var contentString = rule.substring(rule.indexOf("bags contain") + "bags contain".length()).trim();
        if (!"no other bags.".equals(contentString)) {
            var contentBlocks = contentString.split(",");
            for (var block : contentBlocks) {
                var splittedBlock = block.trim().split(" ");
                content.put(splittedBlock[1] + " " + splittedBlock[2], Long.valueOf(splittedBlock[0]));
            }
        }
    }

    public boolean canContain(String color) {
        return content.containsKey(color);
    }

    public String getContainer() {
        return container;
    }

    public Map<String, Long> getContent() {
        return content;
    }
}
