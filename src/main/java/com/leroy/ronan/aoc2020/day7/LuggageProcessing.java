package com.leroy.ronan.aoc2020.day7;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class LuggageProcessing {

    private final List<String> input;
    private final List<LuggageProcessingRule> rules;

    public LuggageProcessing(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        rules = input.stream()
                .map(LuggageProcessingRule::new)
                .collect(Collectors.toList());

    }

    public List<LuggageProcessingRule> getRules() {
        return rules;
    }

    public Set<String> canContainDirectly(String color) {
        var res = new HashSet<String>();
        for (var rule : rules) {
            if (rule.canContain(color)) {
                res.add(rule.getContainer());
            }
        }
        return res;
    }

    public Set<String> getContainableIn(String color) {
        Set<String> res = new HashSet<>();
        for (var rule : rules) {
            if (rule.getContainer().equals(color)) {
                for (var current : rule.getContent().keySet()) {
                    if (!res.contains(current)) {
                        res.add(current);
                        res.addAll(getContainableIn(current));
                    }
                }
            }
        }
        return res;
    }

    public Set<String> canContain(String color) {
        Set<String> res = new HashSet<>();
        for (var rule : rules) {
            var containable = getContainableIn(rule.getContainer());
            if (containable.contains(color)) {
                res.add(rule.getContainer());
            }
        }
        return res;
    }

    public long shouldContainCount(String color) {
        long res = 1;
        for (var rule : rules) {
            if (rule.getContainer().equals(color)) {
                for (var entry : rule.getContent().entrySet()) {
                    res = res + entry.getValue() * shouldContainCount(entry.getKey());
                }
            }
        }
        return res;
    }
}
