package com.leroy.ronan.aoc2020.day12;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class FerryWayPoint {

    private List<String> input;
    private long positionNS;
    private long positionEW;
    private long wayPointNS;
    private long wayPointEW;

    public FerryWayPoint(String source) throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        positionNS = 0;
        positionEW = 0;
        wayPointNS = 0;
        wayPointEW = 0;
    }

    public void setWayPoint(long wayPointNS, long wayPointEW) {
        this.wayPointNS = wayPointNS;
        this.wayPointEW = wayPointEW;
    }


    public void runEvasiveProgram() {
        for (var command : input) {
            var action = command.charAt(0);
            var value = Integer.parseInt(command.substring(1));
            long newWaypointNS;
            long newWaypointEW;
            switch (action) {
                case 'N':
                    wayPointNS = wayPointNS + value;
                    break;
                case 'S':
                    wayPointNS = wayPointNS - value;
                    break;
                case 'E':
                    wayPointEW = wayPointEW + value;
                    break;
                case 'W':
                    wayPointEW = wayPointEW - value;
                    break;
                case 'L':
                    while (value > 0) {
                        newWaypointNS = wayPointEW;
                        newWaypointEW = -1 * wayPointNS;
                        wayPointNS = newWaypointNS;
                        wayPointEW = newWaypointEW;
                        value = value - 90;
                    }
                    break;
                case 'R':
                    while (value > 0) {
                        newWaypointNS = -1 * wayPointEW;
                        newWaypointEW = wayPointNS;
                        wayPointNS = newWaypointNS;
                        wayPointEW = newWaypointEW;
                        value = value - 90;
                    }
                    break;
                case 'F':
                    positionNS = positionNS + wayPointNS * value;
                    positionEW = positionEW + wayPointEW * value;
                    break;
            }
        }
    }

    public long getManhattanDistance() {
        return Math.abs(positionNS) + Math.abs(positionEW);
    }

}
