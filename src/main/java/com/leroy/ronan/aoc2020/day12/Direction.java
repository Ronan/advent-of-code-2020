package com.leroy.ronan.aoc2020.day12;

public enum Direction {
    NORTH(0),
    EAST(90),
    SOUTH(180),
    WEST(270),
    ;

    public int angle;

    Direction(int angle) {
        this.angle = angle;
    }

    public static Direction of(int angle) {
        while(angle >= 360) {
            angle = angle - 360;
        }
        while (angle < 0) {
            angle = angle + 360;
        }
        for (Direction current : Direction.values()) {
            if (angle == current.angle) {
                return current;
            }
        }
        return null;
    }
}
