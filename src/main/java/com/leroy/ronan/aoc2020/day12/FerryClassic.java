package com.leroy.ronan.aoc2020.day12;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static com.leroy.ronan.aoc2020.day12.Direction.EAST;

public class FerryClassic {

    private List<String> input;
    private long positionNS;
    private long positionEW;
    private Direction direction;

    public FerryClassic(String source) throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        positionNS = 0;
        positionEW = 0;
        direction = EAST;
    }

    public void runEvasiveProgram() {
        for (var command : input) {
            var action = command.charAt(0);
            var value = Integer.parseInt(command.substring(1));
            switch (action) {
                case 'N':
                    positionNS = positionNS + value;
                    break;
                case 'S':
                    positionNS = positionNS - value;
                    break;
                case 'E':
                    positionEW = positionEW + value;
                    break;
                case 'W':
                    positionEW = positionEW - value;
                    break;
                case 'L':
                    direction = Direction.of(direction.angle - value);
                    break;
                case 'R':
                    direction = Direction.of(direction.angle + value);
                    break;
                case 'F':
                    switch (direction) {
                        case NORTH:
                            positionNS = positionNS + value;
                            break;
                        case SOUTH:
                            positionNS = positionNS - value;
                            break;
                        case EAST:
                            positionEW = positionEW + value;
                            break;
                        case WEST:
                            positionEW = positionEW - value;
                            break;
                    }
                    break;
            }
        }
    }

    public long getManhattanDistance() {
        return Math.abs(positionNS) + Math.abs(positionEW);
    }
}
