package com.leroy.ronan.aoc2020.day10;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JoltAdapterChain {

    private List<String> input;
    private List<Long> orderedValues;
    private List<Long> adapterTypes;

    public JoltAdapterChain(String source) throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        var adapters = input.stream()
                .map(Long::valueOf)
                .sorted()
                .collect(Collectors.toList());
        orderedValues = new ArrayList<>();
        orderedValues.add(0L);
        orderedValues.addAll(adapters);
        orderedValues.add(adapters.get(adapters.size() - 1) + 3);
        adapterTypes = getAdapterTypes();
    }

    public JoltAdapterChain(long...adapters) {
        orderedValues = Arrays.stream(adapters)
                .boxed()
                .sorted()
                .collect(Collectors.toList());
        adapterTypes = getAdapterTypes();
    }

    private List<Long> getAdapterTypes() {
        List<Long> differences = new ArrayList<>();
        for (int i = 1; i < orderedValues.size(); i++) {
            differences.add(orderedValues.get(i) - orderedValues.get(i - 1));
        }
        return differences;
    }

    public long getAdaptersTypeCount(int type) {
        return adapterTypes.stream()
                .filter(v -> v == type)
                .count();
    }

    public long getAdapterTypesProduct() {
        return getAdaptersTypeCount(1) * getAdaptersTypeCount(3);
    }

    public long getArrangements() {
        System.out.println("-> " + this.toString());
        long res = 0;
        if (adapterTypes.contains(3L) && adapterTypes.indexOf(3L) < adapterTypes.size() - 1) {
            var first = adapterTypes.indexOf(3L);
            var left = orderedValues.subList(0, first + 1);
            var leftChain = new JoltAdapterChain(left.stream().mapToLong(l -> l).toArray());
            var right = orderedValues.subList(first + 1, orderedValues.size());
            var rightChain = new JoltAdapterChain(right.stream().mapToLong(l -> l).toArray());
            res = leftChain.getArrangements() * rightChain.getArrangements();
        } else {
            if (orderedValues.size() == 1) {
                res = res + 1;
            } else if (orderedValues.size() == 2) {
                if (this.isValid()) {
                    res = res + 1;
                }
            } else if (orderedValues.size() == 3) {
                if (this.isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 2)).isValid()) {
                    res = res + 1;
                }
            } else if (orderedValues.size() == 4) {
                if (this.isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 2, 3)).isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 1, 3)).isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 3)).isValid()) {
                    res = res + 1;
                }
            } else if (orderedValues.size() == 5) {
                if (this.isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 2, 3, 4)).isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 1, 3, 4)).isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 1, 2, 4)).isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 3, 4)).isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 2, 4)).isValid()) {
                    res = res + 1;
                }
                if (this.subChain(List.of(0, 1, 4)).isValid()) {
                    res = res + 1;
                }
            } else if (adapterTypes.indexOf(3L) == adapterTypes.size() - 1) {
                res = orderedValues.size() - 2 + orderedValues.size() - 3;
            }
        }
        System.out.println("<- " + res);
        return res;
    }

    private boolean isValid() {
        return adapterTypes.stream().max(Long::compareTo).orElse(-1L) <= 3;
    }

    private JoltAdapterChain subChain(List<Integer> keep) {
        List<Long> values = new ArrayList<>();
        for (int i = 0; i < orderedValues.size(); i++) {
            if (keep.contains(i)) {
                values.add(orderedValues.get(i));
            }
        }
        return new JoltAdapterChain(values.stream().mapToLong(l -> l).toArray());
    }

    @Override
    public String toString() {
        return orderedValues.toString();
    }
}
