package com.leroy.ronan.aoc2020.day1;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class ExpenseReport {

    private List<Long> longs;

    public ExpenseReport(List<Long> longs) {
        this.longs = longs;
    }


    public ExpenseReport(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        longs = Files.lines(path)
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }

    public long addUp() {
        for (var first : longs) {
            for (var second : longs) {
                if (first + second == 2020) {
                    return first * second;
                }
            }
        }
        return 0;
    }

    public long addUp3() {
        for (var first : longs) {
            for (var second : longs) {
                for (var third : longs) {
                    if (first + second + third == 2020) {
                        return first * second * third;
                    }
                }
            }
        }
        return 0;
    }
}
