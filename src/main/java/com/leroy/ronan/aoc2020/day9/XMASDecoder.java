package com.leroy.ronan.aoc2020.day9;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class XMASDecoder {

    private List<String> input;
    private List<Long> numbers;
    private int preamble;

    public XMASDecoder(String source, int preamble) throws IOException, URISyntaxException {
        this.preamble = preamble;
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        numbers = input.stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }

    public boolean isValid(int index) {
        if (index < preamble) {
            return true;
        }
        for (int i = index-preamble; i < index; i++) {
            for (int j = index-preamble; j < index; j++) {
                if (i != j && numbers.get(i) + numbers.get(j) == numbers.get(index)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int getFirstInvalidIndex() {
        for (int i = 0; i < numbers.size(); i++) {
            if (!isValid(i)) {
                return i;
            }
        }
        return -1;
    }
    public long getFirstInvalid() {
        return numbers.get(getFirstInvalidIndex());
    }

    public List<Long> getWeaknessRange() {
        long firstInvalid = getFirstInvalid();
        for (int i = 0; i < numbers.size(); i++) {
            long total = 0;
            int current = i;
            while (total < firstInvalid) {
                total = total + numbers.get(current);
                current = current + 1;
                if (total == firstInvalid) {
                    return numbers.subList(i, current);
                }
            }
        }
        return Collections.emptyList();
    }

    public long getWeakness() {
        var range = getWeaknessRange();
        var min = range.stream().min(Long::compareTo);
        var max = range.stream().max(Long::compareTo);
        return min.map(l -> l + max.get()).orElse(-1L);
    }
}
