package com.leroy.ronan.aoc2020.day2;

public class SecondPolicy implements PasswordValidatorPolicy {

    public boolean validate(int first, int second, char letter, String password) {
        var count = 0;
        if (password.charAt(first-1) == letter) {
            count++;
        }
        if (password.charAt(second-1) == letter) {
            count++;
        }
        return count == 1;
    }
}
