package com.leroy.ronan.aoc2020.day2;

public interface PasswordValidatorPolicy {
    boolean validate(int first, int second, char letter, String password);
}
