package com.leroy.ronan.aoc2020.day2;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PasswordValidator {

    private List<String> input;
    private PasswordValidatorPolicy policy;

    public PasswordValidator() {
        input = Collections.emptyList();
        policy = new FirstPolicy();
    }

    public PasswordValidator(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        policy = new FirstPolicy();
    }

    public void setPolicy(PasswordValidatorPolicy policy) {
        this.policy = policy;
    }

    public boolean validate(String input) {
        var parts = input.split(" ");
        var range = parts[0].split("-");
        var min = Integer.parseInt(range[0]);
        var max = Integer.parseInt(range[1]);
        var letter = parts[1].charAt(0);
        var password = parts[2];

        return policy.validate(min, max, letter, password);
    }

    public long validate(List<String> input) {
        return input.stream()
                .filter(this::validate)
                .count();
    }

    public long validateInput() {
        return validate(input);
    }
}
