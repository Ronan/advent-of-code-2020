package com.leroy.ronan.aoc2020.day2;

public class FirstPolicy implements PasswordValidatorPolicy {

    public boolean validate(int first, int second, char letter, String password) {
        var count = password.chars()
                .filter(c -> c == letter)
                .count();
        return first <= count && count <= second;
    }
}
