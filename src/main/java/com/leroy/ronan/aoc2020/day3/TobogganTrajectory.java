package com.leroy.ronan.aoc2020.day3;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class TobogganTrajectory {

    private final List<String> input;

    public TobogganTrajectory(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
    }

    public char get(int x, int y) {
        var line = input.get(y);
        while (x >= line.length()) {
            x = x - line.length();
        }
        return input.get(y).charAt(x);
    }

    public int countTrees(int right, int down) {
        var x = 0;
        var y = 0;

        var count = 0;
        while (y < input.size()) {
            if (this.get(x, y) == '#') {
                count++;
            }
            x = x + right;
            y = y + down;
        }
        return count;
    }

    public long response() {
        long resp = 1;
        resp = resp * this.countTrees(1, 1);
        resp = resp * this.countTrees(3, 1);;
        resp = resp * this.countTrees(5, 1);;
        resp = resp * this.countTrees(7, 1);;
        resp = resp * this.countTrees(1, 2);;
        return resp;
    }
}
