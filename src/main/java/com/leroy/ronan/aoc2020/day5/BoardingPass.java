package com.leroy.ronan.aoc2020.day5;

import java.util.Objects;

public class BoardingPass {

    private final String code;
    private final long row;
    private final long column;
    private final long seatId;

    public BoardingPass(String code, long row, long column, long seatId) {
        this.code = code;
        this.row = row;
        this.column = column;
        this.seatId = seatId;
    }

    public String getCode() {
        return code;
    }

    public long getRow() {
        return row;
    }

    public long getColumn() {
        return column;
    }

    public long getSeatId() {
        return seatId;
    }

    @Override
    public String toString() {
        return "BoardingPass{" +
               "code='" + code + '\'' +
               ", row=" + row +
               ", column=" + column +
               ", seatId=" + seatId +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardingPass that = (BoardingPass) o;
        return Objects.equals(code, that.code) &&
               Objects.equals(row, that.row) &&
               Objects.equals(column, that.column) &&
               Objects.equals(seatId, that.seatId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, row, column, seatId);
    }
}
