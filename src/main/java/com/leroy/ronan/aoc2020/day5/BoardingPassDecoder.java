package com.leroy.ronan.aoc2020.day5;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class BoardingPassDecoder {

    private List<BoardingPass> input;

    public BoardingPassDecoder() {
        input = Collections.emptyList();
    }

    public BoardingPassDecoder(String source) throws IOException, URISyntaxException {
        super();
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .map(this::decode)
                .collect(Collectors.toList());
    }

    public long getHighestSeatId() {
        return this.input.stream()
                .map(BoardingPass::getSeatId)
                .max(Long::compareTo)
                .orElse(0L);
    }

    public long getAvailableSeatId() {
        var sortedSeatIds = this.input.stream()
                .map(BoardingPass::getSeatId)
                .sorted()
                .collect(Collectors.toList());
        for (int i = 0; i < sortedSeatIds.size() -2; i++) {
            if (sortedSeatIds.get(i) + 1 != sortedSeatIds.get(i+1)) {
                return sortedSeatIds.get(i) + 1;
            }
        }
        return 0;
    }

    public BoardingPass decode(String code) {
        var row = decodeRow(code.substring(0, 7));
        var col = decodeCol(code.substring(7));
        var seatId = row * 8 + col;
        return new BoardingPass(code, row, col, seatId);
    }

    public long decodeRow(String row) {
        var min = 0;
        var max = 127;
        for (char current : row.toCharArray()) {
            var half = (min + max) / 2;
            if (current == 'F') {
                max = half;
            } else if (current == 'B') {
                min = half + 1;
            }
        }
        return min;
    }

    public long decodeCol(String col) {
        var min = 0;
        var max = 7;
        for (char current : col.toCharArray()) {
            var half = (min + max) / 2;
            if (current == 'L') {
                max = half;
            } else if (current == 'R') {
                min = half + 1;
            }
        }
        return min;
    }

}
