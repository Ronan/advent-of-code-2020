package com.leroy.ronan.aoc2020.day15;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MemoryGame {

    private int stop;

    public MemoryGame(int stop) {
        this.stop = stop;
    }


    public long play(List<Long> input) {
        Map<Long, Long> memory = new HashMap<>();
        long last = -1;
        long previous = last;
        for (int i = 0; i < stop; i++) {
            last = getResponse(i, input, memory, last);
            memory.put(previous, (long)i - 1);
            previous = last;
        }
        return last;
    }

    private long getResponse(int index, List<Long> input, Map<Long, Long> memory, long last) {
        var res = 0L;
        if (index < input.size()) {
            res = input.get(index);
        } else if (memory.containsKey(last)) {
            res = index - memory.get(last) - 1;
        }
        return res;
    }
}
