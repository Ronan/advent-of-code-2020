package com.leroy.ronan.aoc2020.day4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NorthPoleCredentials {

    private Map<String, String> content;

    public NorthPoleCredentials(List<String> lines) {
        content = new HashMap<>();
        for (var line : lines) {
            for (var entry : line.split(" ")) {
                var split = entry.split(":");
                set(split[0], split[1]);
            }
        }
    }

    public String get(String key) {
        return content.get(key);
    }

    public void set(String key, String value) {
        content.put(key, value);
    }

    public boolean hasNeededFields() {
        return content.containsKey("byr")
               && content.containsKey("iyr")
               && content.containsKey("eyr")
               && content.containsKey("hgt")
               && content.containsKey("hcl")
               && content.containsKey("ecl")
               && content.containsKey("pid")
                ;
    }

    public boolean isValid() {
        if (!hasNeededFields()) {
            return false;
        } else {
            String[] keys = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};
            for (var key : keys) {
                if (!isValid(key)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isValid(String key) {
        switch (key) {
            case "byr":
                return isValidByr(this.get("byr"));
            case "iyr":
                return isValidIyr(this.get("iyr"));
            case "eyr":
                return isValidEyr(this.get("eyr"));
            case "hgt":
                return isValidHgt(this.get("hgt"));
            case "hcl":
                return isValidHcl(this.get("hcl"));
            case "ecl":
                return isValidEcl(this.get("ecl"));
            case "pid":
                return isValidPid(this.get("pid"));
            default:
                return false;
        }
    }

    public boolean isValidByr(String byr) {
        return isValidNumber(byr, 1920, 2002);
    }

    public boolean isValidIyr(String iyr) {
        return isValidNumber(iyr, 2010, 2020);
    }

    public boolean isValidEyr(String eyr) {
        return isValidNumber(eyr, 2020, 2030);
    }

    public boolean isValidHgt(String hgt) {
        if (hgt.endsWith("cm")) {
            return isValidNumber(hgt.substring(0, hgt.length() - 2), 150, 193);
        }
        if (hgt.endsWith("in")) {
            return isValidNumber(hgt.substring(0, hgt.length() - 2), 59, 76);
        }
        return false;
    }

    public boolean isValidHcl(String hcl) {
        if (!hcl.startsWith("#")) {
            return false;
        } else if (hcl.length() != 7){
            return false;
        } else {
            for (char current : hcl.substring(1).toCharArray()) {
                if (current < '0') {
                    return false;
                } else if ('f' < current) {
                    return false;
                }
            }
            return true;
        }
    }

    public boolean isValidEcl(String ecl) {
        String[] colors = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"};
        for (var color : colors) {
            if (color.equals(ecl)) {
                return true;
            }
        }
        return false;
    }

    public boolean isValidPid(String pid) {
        if (pid.length() != 9) {
            return false;
        } else {
            return isValidNumber(pid);
        }
    }

    private boolean isValidNumber(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isValidNumber(String value, int min, int max) {
        try {
            var valueAsInt = Integer.parseInt(value);
            return min <= valueAsInt && valueAsInt <= max;
        } catch (Exception e) {
            return false;
        }
    }
}
