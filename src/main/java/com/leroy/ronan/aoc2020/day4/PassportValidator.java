package com.leroy.ronan.aoc2020.day4;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PassportValidator {

    private final List<String> input;
    private List<NorthPoleCredentials> credentials;

    public PassportValidator(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());

        credentials = new ArrayList<>();
        List<String> currentLines = new ArrayList<>();
        for (var line : input) {
            if (line.trim().length() == 0) {
                credentials.add(new NorthPoleCredentials(currentLines));
                currentLines = new ArrayList<>();
            } else {
                currentLines.add(line);
            }
        }
        credentials.add(new NorthPoleCredentials(currentLines));
    }

    public List<NorthPoleCredentials> getNorthPoleCredentials() {
        return credentials;
    }

    public long count(Predicate<NorthPoleCredentials> predicate) {
        return credentials.stream()
                .filter(predicate)
                .count();
    }
}
