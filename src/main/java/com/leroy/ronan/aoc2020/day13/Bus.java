package com.leroy.ronan.aoc2020.day13;

import java.util.Objects;

public class Bus {

    private final long index;
    private final long number;

    public Bus(long index, long number) {
        this.index = index;
        this.number = number;
    }

    public long getIndex() {
        return index;
    }

    public long getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "Bus{" +
               "index=" + index +
               ", number=" + number +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bus bus = (Bus) o;
        return index == bus.index &&
               number == bus.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, number);
    }
}
