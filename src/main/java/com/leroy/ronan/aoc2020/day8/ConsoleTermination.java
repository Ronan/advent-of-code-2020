package com.leroy.ronan.aoc2020.day8;

public enum ConsoleTermination {
    LOOP,
    NORMAL,
    ;
}
