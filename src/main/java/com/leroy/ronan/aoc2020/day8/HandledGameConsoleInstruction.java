package com.leroy.ronan.aoc2020.day8;

public class HandledGameConsoleInstruction {

    private String instruction;
    private ConsoleOperation operation;
    private final int value;

    public HandledGameConsoleInstruction(String instruction) {
        this.instruction = instruction;
        this.operation = ConsoleOperation.valueOf(instruction.substring(0, 3).toUpperCase());
        this.value = Integer.parseInt(instruction.split(" ") [1]);
    }

    public ConsoleOperation getOperation() {
        return this.operation;
    }

    public void setOperation(ConsoleOperation operation) {
        this.operation = operation;
    }

    public int getValue() {
        return this.value;
    }
}
