package com.leroy.ronan.aoc2020.day8;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.leroy.ronan.aoc2020.day8.ConsoleOperation.*;
import static com.leroy.ronan.aoc2020.day8.ConsoleTermination.*;

public class HandledGameConsole {

    private List<String> input;
    private List<HandledGameConsoleInstruction> instructions;
    private int accumulator;

    public HandledGameConsole() {
        this.accumulator = 0;
    }

    public HandledGameConsole(String source) throws IOException, URISyntaxException {
        this();
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        this.loadInput();
    }

    public void loadInput() {
        instructions = input.stream().map(HandledGameConsoleInstruction::new).collect(Collectors.toList());
    }

    public int getAccumulator() {
        return this.accumulator;
    }

    public ConsoleTermination run() {
        int currentLine = 0;
        accumulator = 0;
        List<Integer> executedLines = new ArrayList<>();
        ConsoleTermination termination = null;
        while (termination == null) {
            executedLines.add(currentLine);
            var current = instructions.get(currentLine);
            switch (current.getOperation()) {
                case NOP:
                    currentLine = currentLine + 1;
                    break;
                case ACC:
                    accumulator = accumulator + current.getValue();
                    currentLine = currentLine + 1;
                    break;
                case JMP:
                    currentLine = currentLine + current.getValue();
                    break;
            }
            if (executedLines.contains(currentLine)) {
                termination = LOOP;
            }
            if (currentLine == input.size()) {
                termination = NORMAL;
            }

        }
        return termination;
    }

    public void fix(int i) {
        var instruction = instructions.get(i);
        switch (instruction.getOperation()) {
            case NOP:
                instruction.setOperation(JMP);
                break;
            case JMP:
                instruction.setOperation(NOP);
                break;
            case ACC:
                break;
        }
    }

    public int fix() {
        ConsoleTermination termination = null;
        int fixLine = 0;
        while (termination != NORMAL) {
            this.loadInput();
            this.fix(fixLine);
            termination = this.run();
            accumulator = this.getAccumulator();
            fixLine = fixLine + 1;
        }
        return accumulator;
    }
}
