package com.leroy.ronan.aoc2020.day6;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomsDeclarationFormGroup {

    private List<String> responses;

    public CustomsDeclarationFormGroup(List<String> responses) {
        this.responses = responses;
    }

    public long getAnyCount() {
        var all = String.join("", responses);
        Map<Character, Long> counts = new HashMap<>();
        for (char current : all.toCharArray()) {
            if (!counts.containsKey(current)) {
                counts.put(current, 0L);
            }
            counts.put(current, counts.get(current));
        }
        return counts.keySet().size();
    }

    public long getAllCount() {
        var res = 0L;
        for (char c = 'a'; c <='z'; c++) {
            var search = String.valueOf(c);
            if (responses.stream().allMatch(s -> s.contains(search))) {
                res++;
            }
        }
        return res;
    }
}
