package com.leroy.ronan.aoc2020.day6;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CustomsDeclarationForms {

    private final List<String> input;
    private List<CustomsDeclarationFormGroup> groups;

    public CustomsDeclarationForms(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        groups = new ArrayList<>();
        List<String> currentGroup = new ArrayList<>();
        for (var line : input) {
            if ("".equals(line.trim())) {
                if (currentGroup.size() > 0) {
                    groups.add(new CustomsDeclarationFormGroup(currentGroup));
                }
                currentGroup = new ArrayList<>();
            } else {
                currentGroup.add(line);
            }
        }
        if (currentGroup.size() > 0) {
            groups.add(new CustomsDeclarationFormGroup(currentGroup));
        }
    }

    public List<CustomsDeclarationFormGroup> getGroups() {
        return this.groups;
    }

    public long sumAnyCount() {
        return groups.stream()
                .mapToLong(CustomsDeclarationFormGroup::getAnyCount)
                .sum();
    }

    public long sumAllCount() {
        return groups.stream()
                .mapToLong(CustomsDeclarationFormGroup::getAllCount)
                .sum();
    }
}
