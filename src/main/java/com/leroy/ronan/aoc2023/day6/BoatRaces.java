package com.leroy.ronan.aoc2023.day6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BoatRaces {

    private List<BoatRace> input = new ArrayList<>();
    private BoatRace bigRace;

    public BoatRaces(Stream<String> lines) {
        var data = lines.map(
                l -> {
                    var parts = l.split(":")[1].trim().split(" ");
                    return Arrays.stream(parts).filter(s -> !s.isBlank()).map(Long::parseLong).toList();
                }
        ).toList();
        for (int i = 0; i < data.get(0).size(); i++) {
            input.add(new BoatRace(data.get(0).get(i), data.get(1).get(i)));
        }
        this.bigRace = new BoatRace(
                Long.parseLong(data.get(0).stream().map(l -> Long.toString(l)).collect(Collectors.joining())),
                Long.parseLong(data.get(1).stream().map(l -> Long.toString(l)).collect(Collectors.joining()))
        );
    }

    public long compute() {
        return input.stream()
                .mapToLong(BoatRace::margin)
                .reduce(1L, (l1, l2) -> l1 * l2);
    }

    public long computeBig() {
        return bigRace.margin();
    }

    public record BoatRace(long time, long distance) {

        public long margin() {
            long res = 0;
            for (long speed = 1; speed < time; speed++ ) {
                long dist = speed * (time - speed);
                if (dist > distance) {
                    res++;
                }
            }
            return res;
        }
    }
}
