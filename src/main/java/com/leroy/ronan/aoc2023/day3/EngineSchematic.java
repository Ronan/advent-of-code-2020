package com.leroy.ronan.aoc2023.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class EngineSchematic {

    private final String[] input;

    public EngineSchematic(Stream<String> input) {
        this.input = input
                .map(line -> "."+line+".")
                .toArray(String[]::new);
    }

    public long sum() {
        long res = 0;
        for (int y = 0; y < input.length; y++) {
            long current = 0;
            for (int x = 0; x < input[y].length(); x++) {
                var c = input[y].charAt(x);
                if ('0' <= c && c <= '9') {
                    current = current * 10 + (c - '0');
                    System.out.print(c);
                } else {
                    if (current > 0) {
                        if (shouldBeAdded(current, x, y)) {
                            res += current;
                        }
                    }
                    current = 0;
                    System.out.print(c);
                }
            }
            System.out.print("\n");
        }
        return res;
    }

    protected boolean shouldBeAdded(long current, int x, int y) {
        int minX = x - String.valueOf(current).length() -1;
        for (int curX = minX; curX <= x; curX++) {
            if (this.charAt(curX, y-1) != '.') {
                return true;
            }
        }
        for (int curX = minX; curX <= x; curX++) {
            if (this.charAt(curX, y+1) != '.') {
                return true;
            }
        }
        if (this.charAt(minX, y) != '.') {
            return true;
        }
        if (this.charAt(x, y) != '.') {
            return true;
        }
        return false;
    }

    private char charAt(int x, int y) {
        if (y < 0 || input.length <= y) {
            return '.';
        }
        return input[y].charAt(x);
    }

    public long gearRatio() {
        long res = 0;
        for (int y = 0; y < input.length; y++) {
            for (int x = 0; x < input[y].length(); x++) {
                var c = input[y].charAt(x);
                if (c == '*') {
                    List<Long> parts = new ArrayList<>();
                    System.out.println("Gear at "+x+","+y);
                    if (Character.isDigit(charAt(x, y - 1))) {
                        parts.add(this.partAt(x, y - 1));
                    } else {
                        if (Character.isDigit(charAt(x - 1, y - 1))) {
                            parts.add(this.partAt(x-1, y - 1));
                        }
                        if (Character.isDigit(charAt(x + 1, y - 1))) {
                            parts.add(this.partAt(x+1, y - 1));
                        }
                    }
                    if (Character.isDigit(charAt(x, y + 1))) {
                        parts.add(this.partAt(x, y + 1));
                    } else {
                        if (Character.isDigit(charAt(x - 1, y + 1))) {
                            parts.add(this.partAt(x-1, y + 1));
                        }
                        if (Character.isDigit(charAt(x + 1, y + 1))) {
                            parts.add(this.partAt(x+1, y + 1));
                        }
                    }
                    if (Character.isDigit(charAt(x-1, y))) {
                        parts.add(this.partAt(x-1, y));
                    }
                    if (Character.isDigit(charAt(x+1, y))) {
                        parts.add(this.partAt(x+1, y));
                    }
                    if (parts.size() == 2) {
                        System.out.println("Added Gear at "+x+","+y);
                        res += parts.get(0) * parts.get(1);
                    }
                }
            }
        }
        return res;
    }

    protected long partAt(int x, int y) {
        int curX = x;
        while (Character.isDigit(this.charAt(curX -1, y))) {
            curX = curX -1;
        }
        long res = 0;
        while (Character.isDigit(this.charAt(curX, y))) {
            res = res * 10 + (this.charAt(curX, y) - '0');
            curX = curX + 1;
        }
        if (res == 0) {
            System.out.println("really ?");
        }
        return res;
    }
}


/*



 */