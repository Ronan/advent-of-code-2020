package com.leroy.ronan.aoc2023.day4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ScratchCards {

    private final List<ScratchCard> input;

    public ScratchCards(Stream<String> input) {
        this.input = input.map(ScratchCard::parse).toList();
    }

    public long sum() {
        return input.stream()
                .mapToLong(ScratchCard::value)
                .sum();
    }

    public long count() {
        List<Long> ownedCards = new ArrayList<>();
        for (int i = 0; i < input.size(); i++) {
            ownedCards.add(1L);
        }
        for (int j = 0; j < input.size(); j++) {
            var current = input.get(j);
            for (int i = j + 1; i < j + 1 + current.win() && i < ownedCards.size(); i++) {
                ownedCards.set(i, ownedCards.get(i) + ownedCards.get(j));
            }
        }
        return ownedCards.stream().mapToLong(Long::longValue).sum();
    }

    public record ScratchCard(int id, Set<Long> wins, Set<Long> owned) {
        public static ScratchCard parse(String input) {
            var parts = input.split(":");
            var id = Integer.parseInt(parts[0].replaceAll("Card", " ").trim());

            var wins = Arrays.stream(parts[1].trim().split("\\|")[0].split(" "))
                    .filter(s -> !s.isBlank())
                    .map(Long::parseLong)
                    .collect(Collectors.toSet());
            var owned = Arrays.stream(parts[1].trim().split("\\|")[1].split(" "))
                    .filter(s -> !s.isBlank())
                    .map(Long::parseLong)
                    .collect(Collectors.toSet());
            return new ScratchCard(id, wins, owned);
        }

        public long value() {
            int res = 0;
            for (var current : owned) {
                if (wins.contains(current)) {
                    if (res == 0) {
                        res = 1;
                    } else {
                        res = res *2;
                    }
                }
            }
            return res;
        }

        public int win() {
            int res = 0;
            for (var current : owned) {
                if (wins.contains(current)) {
                    res = res +1;
                }
            }
            return res;
        }
    }
}
