package com.leroy.ronan.aoc2023.day2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CubeGame {

    protected final Collection<Game> games;

    public CubeGame(String input) {
        this.games = parse(input.lines());
    }

    public CubeGame(Path path) throws IOException {
        this.games = parse(Files.lines(path));
    }

    private Collection<Game> parse(Stream<String> lines) {
        return lines
                .map(Game::parse)
                .collect(Collectors.toSet());
    }

    public long sum(String input) {
        var hand = Hand.parse(input);
        return games.stream()
                .filter(game -> game.hands.stream()
                        .allMatch(h -> h.blue() <= hand.blue() && h.green() <= hand.green() && h.red() <= hand.red())
                )
                .mapToLong(Game::id)
                .sum();
    }

    public long power() {
        return games.stream()
                .map(Game::minimize)
                .mapToLong(game -> game.blue() * game.green() * game.red())
                .sum();
    }

    public Game getById(long i) {
        return games.stream()
                .filter(game -> game.id == i)
                .findFirst()
                .orElseThrow();
    }

    public record Game(long id, Collection<Hand> hands) {
        public static Game parse(String line) {
            var parts = line.split(":");
            var id = Integer.parseInt(parts[0].trim().split(" ")[1]);
            var hands = Arrays.stream(parts[1].trim().split(";"))
                    .map(Hand::parse)
                    .collect(Collectors.toSet());
            return new Game(id, hands);
        }

        public Hand minimize() {
            long blue = 0;
            long green = 0;
            long red = 0;
            for (var hand : hands) {
                blue = Math.max(blue, hand.blue());
                green = Math.max(green, hand.green());
                red = Math.max(red, hand.red());
            }
            return hand().red(red).green(green).blue(blue);
        }
    }

    public record Hand(long blue, long green, long red) {
        public static Hand parse(String hand) {
            long blue = 0;
            long green = 0;
            long red = 0;

            var parts = hand.trim().split(",");
            for (var part : parts) {
                var color = part.trim().split(" ");
                switch (color[1]) {
                    case "blue" -> blue = Long.parseLong(color[0]);
                    case "green" -> green = Long.parseLong(color[0]);
                    case "red" -> red = Long.parseLong(color[0]);
                }
            }

            return new Hand(blue, green, red);
        }

        public Hand blue(long i) {
            return new Hand(i, green, red);
        }
        public Hand green(long i) {
            return new Hand(blue, i, red);
        }
        public Hand red(long i) {
            return new Hand(blue, green, i);
        }
    }

    public static Game game(int id, Hand... hands) {
        return new Game(id, Set.of(hands));
    }

    public static Hand hand() {
        return new Hand(0, 0, 0);
    }
}
