package com.leroy.ronan.aoc2023.day1;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Calibrator {

    private List<String> input;

    public Calibrator(List<String> input) {
        this.input = input;
    }

    public Calibrator(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        try (var lines = Files.lines(path)) {
            this.input = lines.toList();
        }
    }

    public long calibrate() {
        return calibrate(false);
    }

    public long calibrate(boolean complex) {
        if (complex) {
            return input.stream()
                    .mapToLong(this::toLineValueComplex)
                    .sum();
        } else {
            return input.stream().mapToLong(this::toLineValue).sum();
        }
    }

    private long toLineValueComplex(String s) {
        var first = findFirst(s);
        var last = findLast(s);
        return first * 10 + last;
    }

    private long findFirst(String s) {
        if (Character.isDigit(s.charAt(0))) {
            return s.charAt(0) - '0';
        } else {
            for (var digit : Digit.values()) {
                if (s.startsWith(digit.getName())) {
                    return digit.getValue();
                }
            }
        }
        return findFirst(s.substring(1));
    }

    private long findLast(String s) {
        if (Character.isDigit(s.charAt(s.length() - 1))) {
            return s.charAt(s.length() - 1) - '0';
        } else {
            for (var digit : Digit.values()) {
                if (s.endsWith(digit.getName())) {
                    return digit.getValue();
                }
            }
        }
        return findLast(s.substring(0, s.length() - 1));
    }

    private long toLineValue(String s) {
        long first = -1L;
        long last = -1L;
        for (char current : s.toCharArray()) {
            if (Character.isDigit(current)) {
                if (first < 0) {
                    first = current - '0';
                }
                last = current - '0';
            }
        }
        return first *10 + last;
    }
}
