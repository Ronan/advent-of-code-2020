package com.leroy.ronan.aoc2023.day5;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class SeedMap {

    private List<Long> seeds;
    private final List<TransformationMap> maps = new ArrayList<>();
    private final long start = System.currentTimeMillis();

    public SeedMap(Stream<String> input) {
        List<Range> currentList = new ArrayList<>();
        String currentId = null;

        for (var current : input.toList()) {
            if (current.startsWith("seeds:")) {
                this.seeds = Arrays.stream(current.replaceAll("seeds:", "").trim().split(" "))
                        .map(Long::parseLong).toList();
            } else if (current.trim().contains("map:")) {
                currentList = new ArrayList<>();
                currentId = current.trim().split(" ")[0];
            } else if (!current.isBlank()) {
                var range = new Range("", Arrays.stream(current.split(" "))
                        .mapToLong(Long::parseLong)
                        .toArray());
                currentList.add(range);
            } else {
                if (currentId != null) {
                    maps.add(new TransformationMap(currentId, currentList));
                    currentId = null;
                    currentList = new ArrayList<>();
                }
            }
        }
        maps.add(new TransformationMap(currentId, currentList));
    }

    public long getLowestLocationNumber() {
        return this.seeds.stream()
                .map(this::getLocationNumber)
                .min(Long::compareTo)
                .orElseThrow();
    }

    public long getLowestLocationNumberExt() {
        log("Start - " + (this.seeds.size() /2) + " seeds range");

        List<Range> seedRanges = new ArrayList<>();
        for (int i = 0; i < this.seeds.size(); i = i + 2) {
            long start = this.seeds.get(i);
            long length = this.seeds.get(i + 1);
            seedRanges.add(new Range(String.valueOf((i+1) / 2), start, start, length));
        }
        long seedsCount = seedRanges.stream().mapToLong(Range::length).sum();
        log("Seed count : " + seedsCount);

        return seedRanges.stream()
                .parallel()
                .mapToLong(this::getMinLocationNumber)
                .min()
                .orElseThrow();
    }

    private long getMinLocationNumber(Range range) {
        log("Start... - " + range.id());
        log("From " + range.source() + " to " + (range.source() + range.length()));
        long res = LongStream.range(range.source(), range.source() + range.length())
                .parallel()
                .map(this::getLocationNumber)
                .min()
                .orElseThrow();
        log("Done.... : " + range.id());
        log("Min location is : " + res);
        return res;
    }

    private void log(String message) {
        var duration = Duration.ofMillis(System.currentTimeMillis() - this.start).getSeconds();
        String formattedDuration = String.format("%02d:%02d:%02d", duration / 3600, (duration % 3600) / 60, (duration % 60));
        System.out.println(formattedDuration + " - " + message);
    }

    private long getLocationNumber(long seed) {
        long current = seed;
        for (var map : maps) {
            current = transform(current, map.ranges());
        }
        return current;
    }

    private long transform(long currentValue, List<Range> currentRanges) {
        for (var range : currentRanges) {
            if (range.contains(currentValue)) {
                return range.transform(currentValue);
            }
        }
        return currentValue;
    }

    public record Range(String id, long destination, long source, long length){
        Range(String id, long ... values) {
            this(id, values[0], values[1], values[2]);
        }

        public boolean contains(long value) {
            return source <= value && value < source + length;
        }

        public long transform(long value) {
            return destination + value - source;
        }
    }

    public record TransformationMap(String id, List<Range> ranges){
        public TransformationMap {
            ranges = List.of(ranges.toArray(new Range[0]));
        }
    }
}
