package com.leroy.ronan.aoc2020.day9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class XMASDecoderShould {

    @Test
    public void find_127_as_first_invalid_number_of_samples() throws IOException, URISyntaxException {
        var decoder = new XMASDecoder("day-9-sample.txt", 5);
        Assertions.assertTrue(decoder.isValid(0));
        Assertions.assertTrue(decoder.isValid(5));
        Assertions.assertTrue(decoder.isValid(6));
        Assertions.assertFalse(decoder.isValid(14));
        Assertions.assertEquals(14, decoder.getFirstInvalidIndex());
        Assertions.assertEquals(127L, decoder.getFirstInvalid());
        Assertions.assertEquals(List.of(15L, 25L, 47L, 40L), decoder.getWeaknessRange());
        Assertions.assertEquals(62L, decoder.getWeakness());
    }

    @Test
    public void find_127_as_first_invalid_number_of_input_numbers() throws IOException, URISyntaxException {
        var decoder = new XMASDecoder("day-9-input.txt", 25);
        Assertions.assertEquals(537, decoder.getFirstInvalidIndex());
        Assertions.assertEquals(20874512L, decoder.getFirstInvalid());
        Assertions.assertEquals(3012420L, decoder.getWeakness());
    }
}
