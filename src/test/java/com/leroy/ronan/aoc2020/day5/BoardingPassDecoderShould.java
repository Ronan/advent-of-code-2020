package com.leroy.ronan.aoc2020.day5;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoardingPassDecoderShould {

    private static BoardingPassDecoder decoder;

    @BeforeAll
    public static void setup() throws IOException, URISyntaxException {
        decoder = new BoardingPassDecoder("day-5-input.txt");
    }

    @Test
    public void decode_boarding_passes() {
        assertEquals(new BoardingPass("FBFBBFFRLR", 44, 5, 357), decoder.decode("FBFBBFFRLR"));
        assertEquals(new BoardingPass("BFFFBBFRRR", 70, 7, 567), decoder.decode("BFFFBBFRRR"));
        assertEquals(new BoardingPass("FFFBBBFRRR", 14, 7, 119), decoder.decode("FFFBBBFRRR"));
        assertEquals(new BoardingPass("BBFFBBFRLL", 102, 4, 820), decoder.decode("BBFFBBFRLL"));
    }

    @Test
    public void decode_rows() {
        assertEquals(44, decoder.decodeRow("FBFBBFF"));
        assertEquals(70, decoder.decodeRow("BFFFBBF"));
        assertEquals(14, decoder.decodeRow("FFFBBBF"));
        assertEquals(102, decoder.decodeRow("BBFFBBF"));
    }

    @Test
    public void decode_cols() {
        assertEquals(5, decoder.decodeCol("RLR"));
        assertEquals(7, decoder.decodeCol("RRR"));
        assertEquals(4, decoder.decodeCol("RLL"));
    }

    @Test
    public void find_highest_seat_id() {
        assertEquals(919, decoder.getHighestSeatId());
    }

    @Test
    public void find_available_seat_id() {
        assertEquals(642, decoder.getAvailableSeatId());
    }
}
