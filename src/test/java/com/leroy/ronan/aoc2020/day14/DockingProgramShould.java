package com.leroy.ronan.aoc2020.day14;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class DockingProgramShould {

    @Test
    public void produce_a_sum_of_165_for_sample_input() throws IOException, URISyntaxException {
        var program = new DockingProgram("day-14-sample.txt");
        program.run();
        Assertions.assertEquals(165, program.getMemorySum());
    }

    @Test
    public void mask_11_into_73() {
        Assertions.assertEquals(73, DockingProgram.applyMask("11", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"));
    }

    @Test
    public void represent_11_as_000000000000000000000000000000001011() {
        Assertions.assertEquals("000000000000000000000000000000001011", DockingProgram.toBinary("11"));
    }

    @Test
    public void represent_101_as_000000000000000000000000000001100101() {
        Assertions.assertEquals("000000000000000000000000000001100101", DockingProgram.toBinary("101"));
    }

    @Test
    public void convert_000000000000000000000000000000001011_into_11() {
        Assertions.assertEquals(11, DockingProgram.toLong("000000000000000000000000000000001011"));
    }

    @Test
    public void convert_000000000000000000000000000001100101_into_101() {
        Assertions.assertEquals(101, DockingProgram.toLong("000000000000000000000000000001100101"));
    }

    @Test
    public void produce_a_sum_of_165_for_real_input() throws IOException, URISyntaxException {
        var program = new DockingProgram("day-14-input.txt");
        program.run();
        Assertions.assertEquals(12135523360904L, program.getMemorySum());
    }

    @Test
    public void produce_a_sum_of_208_for_sample_input_v2() throws IOException, URISyntaxException {
        var program = new DockingProgram("day-14-sample-2.txt");
        program.setVersion(2);
        program.run();
        Assertions.assertEquals(208, program.getMemorySum());
    }

    @Test
    public void produce_4_indexes_from_42_masked_with_000000000000000000000000000000X1001X() {
        Assertions.assertEquals(
                List.of(
                        "000000000000000000000000000000011010",
                        "000000000000000000000000000000011011",
                        "000000000000000000000000000000111010",
                        "000000000000000000000000000000111011"
                ),
                DockingProgram.getMaskedIndexes(
                        "000000000000000000000000000000101010",
                        "000000000000000000000000000000X1001X"
                )
        );
    }

    @Test
    public void combine_000000000000000000000000000000101010_and_000000000000000000000000000000X1001X_into_000000000000000000000000000000X1101X() {
        Assertions.assertEquals(
                "000000000000000000000000000000X1101X",
                DockingProgram.combine(
                        "000000000000000000000000000000101010",
                        "000000000000000000000000000000X1001X"
                )
        );
    }

    @Test
    public void produce_a_sum_of_208_for_real_input_v2() throws IOException, URISyntaxException {
        var program = new DockingProgram("day-14-input.txt");
        program.setVersion(2);
        program.run();
        Assertions.assertEquals(2741969047858L, program.getMemorySum());
    }
}
