package com.leroy.ronan.aoc2020.day12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class FerryEvasiveProgramShould {

    @Test
    public void finish_at_17_8_with_sample_input() throws IOException, URISyntaxException {
        FerryClassic ferry = new FerryClassic("day-12-sample.txt");
        ferry.runEvasiveProgram();
        Assertions.assertEquals(25, ferry.getManhattanDistance());
    }

    @Test
    public void finish_at_a_distance_of_796_with_input() throws IOException, URISyntaxException {
        FerryClassic ferry = new FerryClassic("day-12-input.txt");
        ferry.runEvasiveProgram();
        Assertions.assertEquals(796, ferry.getManhattanDistance());
    }

    @Test
    public void finish_at_214_72_with_sample_input() throws IOException, URISyntaxException {
        FerryWayPoint ferry = new FerryWayPoint("day-12-sample.txt");
        ferry.setWayPoint(1, 10);
        ferry.runEvasiveProgram();
        Assertions.assertEquals(286, ferry.getManhattanDistance());
    }

    @Test
    public void finish_at_a_distance_of_XXX_with_input() throws IOException, URISyntaxException {
        FerryWayPoint ferry = new FerryWayPoint("day-12-input.txt");
        ferry.setWayPoint(1, 10);
        ferry.runEvasiveProgram();
        Assertions.assertEquals(39446, ferry.getManhattanDistance());
    }
}
