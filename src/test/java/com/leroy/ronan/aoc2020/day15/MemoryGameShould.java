package com.leroy.ronan.aoc2020.day15;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class MemoryGameShould {

    private MemoryGame game2020 = new MemoryGame(2020);
    private MemoryGame game30M = new MemoryGame(30000000);

    @Test
    public void produce_436_from_0_3_6() {
        Assertions.assertEquals(436, game2020.play(List.of(0L,3L,6L)));
    }

    @Test
    public void produce_expected_responses() {
        Assertions.assertEquals(1, game2020.play(List.of(1L,3L,2L)));
        Assertions.assertEquals(10, game2020.play(List.of(2L,1L,3L)));
        Assertions.assertEquals(27, game2020.play(List.of(1L,2L,3L)));
        Assertions.assertEquals(78, game2020.play(List.of(2L,3L,1L)));
        Assertions.assertEquals(438, game2020.play(List.of(3L,2L,1L)));
        Assertions.assertEquals(1836, game2020.play(List.of(3L,1L,2L)));
    }

    @Test
    public void produce_a_response_for_puzzle_input() {
        Assertions.assertEquals(1238, game2020.play(List.of(9L,6L,0L,10L,18L,2L,1L)));
    }

    @Test
    public void produce_expected_responses_for_part_2() {
        Assertions.assertEquals(175594, game30M.play(List.of(0L,3L,6L)));
        Assertions.assertEquals(2578, game30M.play(List.of(1L,3L,2L)));
        Assertions.assertEquals(3544142, game30M.play(List.of(2L,1L,3L)));
        Assertions.assertEquals(261214, game30M.play(List.of(1L,2L,3L)));
        Assertions.assertEquals(6895259, game30M.play(List.of(2L,3L,1L)));
        Assertions.assertEquals(18, game30M.play(List.of(3L,2L,1L)));
        Assertions.assertEquals(362, game30M.play(List.of(3L,1L,2L)));
    }

    @Test
    public void produce_a_response_for_puzzle_input_part_2() {
        Assertions.assertEquals(3745954, game30M.play(List.of(9L,6L,0L,10L,18L,2L,1L)));
    }
}
