package com.leroy.ronan.aoc2020.day6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class CustomsDeclarationFormsShould {

    @Test
    public void sum_sample_counts() throws IOException, URISyntaxException {
        var forms = new CustomsDeclarationForms("day-6-sample.txt");
        Assertions.assertEquals(5, forms.getGroups().size());
        Assertions.assertEquals(11, forms.sumAnyCount());
        Assertions.assertEquals(6, forms.sumAllCount());
    }

    @Test
    public void sum_input_counts() throws IOException, URISyntaxException {
        var forms = new CustomsDeclarationForms("day-6-input.txt");
        Assertions.assertEquals(504, forms.getGroups().size());
        Assertions.assertEquals(7027, forms.sumAnyCount());
        Assertions.assertEquals(3579, forms.sumAllCount());
    }

}
