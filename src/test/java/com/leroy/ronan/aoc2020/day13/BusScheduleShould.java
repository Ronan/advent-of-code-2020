package com.leroy.ronan.aoc2020.day13;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Set;

public class BusScheduleShould {

    @Test
    public void select_bus_59_at_944_from_939() throws IOException, URISyntaxException {
        BusSchedule schedule = new BusSchedule("day-13-sample.txt");
        Assertions.assertEquals(59, schedule.getNextBusNumber());
        Assertions.assertEquals(944, schedule.getNextTime());
        Assertions.assertEquals(295, schedule.getPart1Response());
        Assertions.assertEquals(
                Set.of(
                        new Bus(0,7),
                        new Bus(1,13),
                        new Bus(4,59),
                        new Bus(6,31),
                        new Bus(7,19)
                ),
                schedule.getBuses()
        );
        Assertions.assertEquals(939, schedule.getCurrentTime());
        Assertions.assertEquals(944, schedule.getTimeForNext(59, schedule.getCurrentTime()));
        Assertions.assertEquals(
                Map.of(
                        new Bus(0,7), 945L,
                        new Bus(1,13), 949L,
                        new Bus(4,59), 944L,
                        new Bus(6,31), 961L,
                        new Bus(7,19), 950L
                ),
                schedule.getScheduleAt(939)
            );

        Assertions.assertFalse(schedule.isPerfectAt(939));
        Assertions.assertEquals(1068781, schedule.getTimeForNext(7, 1068781));
        Assertions.assertTrue(schedule.isPerfectAt(1068781));
        Assertions.assertEquals(1068781, schedule.findPerfect());
    }

    @Test
    public void select_bus_from_input() throws IOException, URISyntaxException {
        BusSchedule schedule = new BusSchedule("day-13-input.txt");
        Assertions.assertEquals(37, schedule.getNextBusNumber());
        Assertions.assertEquals(1000517, schedule.getNextTime());
        Assertions.assertEquals(296, schedule.getPart1Response());
        Assertions.assertEquals(535296695251210L, schedule.findPerfect());
    }

}
