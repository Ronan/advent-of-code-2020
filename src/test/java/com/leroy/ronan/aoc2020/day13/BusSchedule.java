package com.leroy.ronan.aoc2020.day13;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class BusSchedule {

    private List<String> input;
    private long currentTime;
    private HashSet<Bus> buses;

    public BusSchedule(String source) throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        input = Files.lines(path)
                .collect(Collectors.toList());
        currentTime = Long.parseLong(input.get(0));
        this.buses = new HashSet<>();
        String[] buses = input.get(1).split(",");
        for (int i = 0; i < buses.length; i++){
            var busStr = buses[i];
            if (!"x".equals(busStr)) {
                this.buses.add(new Bus(i, Long.parseLong(busStr)));
            }
        }
    }

    public long getPart1Response() {
        return (getNextTime() - currentTime) * getNextBusNumber();
    }

    public Set<Bus> getBuses() {
        return this.buses;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public long getTimeForNext(long busNumber, long at) {
        if (at % busNumber == 0) {
            return at;
        } else {
            return ((at / busNumber) + 1) * busNumber;
        }
    }

    public Map<Bus, Long> getScheduleAt(long at) {
        HashMap<Bus, Long> res = new HashMap<>();
        for (Bus bus : buses) {
            res.put(bus, getTimeForNext(bus.getNumber(), at));
        }
        return res;
    }

    public long getNextBusNumber() {
        long at = this.currentTime;
        return getNextBusNumberAt(at);
    }

    private long getNextBusNumberAt(long at) {
        return getScheduleAt(at).entrySet().stream()
                .min(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .map(Bus::getNumber)
                .orElse(-1L);
    }

    public long getNextTime() {
        long at = this.currentTime;
        return getNextTimeAt(at);
    }

    private long getNextTimeAt(long at) {
        return getScheduleAt(at).entrySet().stream()
                .min(Map.Entry.comparingByValue())
                .map(Map.Entry::getValue)
                .orElse(-1L);
    }

    public boolean isPerfectAt(long at) {
        var schedule = getScheduleAt(at);
        boolean res = true;
        for (var entry : schedule.entrySet()) {
            if (at + entry.getKey().getIndex() != entry.getValue()) {
                res = false;
            }
        }
        return res;
    }

    public long findPerfect() {
        long step = 1;
        long t = 1;
        for (Bus current : buses) {
            while(!isDeparting(current, t + current.getIndex())) {
                t = t + step;
            }
            step = step * current.getNumber();
        }
        return t;
    }

    private boolean isDeparting(Bus current, long at) {
        return at % current.getNumber() == 0;
    }
}
