package com.leroy.ronan.aoc2020.day11;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class SeatingSystemShould {

    @Test
    public void not_change_before_first_round() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-sample.txt");
        Assertions.assertEquals(0, seats.getNextOccupiedSeatsCount());
        Assertions.assertEquals(71, seats.getNonOccupiedSeatsCount());
    }

    @Test
    public void have_all_seats_occupied_after_first_round() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-sample.txt");
        seats.step();
        Assertions.assertEquals(71, seats.getNextOccupiedSeatsCount());
        Assertions.assertEquals(0, seats.getNonOccupiedSeatsCount());
    }

    @Test
    public void have_some_seats_occupied_after_secound_round() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-sample.txt");
        seats.step();
        seats.step();
        Assertions.assertEquals(20, seats.getNextOccupiedSeatsCount());
        Assertions.assertEquals(51, seats.getNonOccupiedSeatsCount());
    }

    @Test
    public void stabilize_with_37_occupied_seats() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-sample.txt");
        seats.stabilize();
        Assertions.assertEquals(37, seats.getNextOccupiedSeatsCount());
        Assertions.assertEquals(34, seats.getNonOccupiedSeatsCount());
    }

    @Test
    public void stabilize_real_input() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-input.txt");
        seats.stabilize();
        Assertions.assertEquals(2166, seats.getNextOccupiedSeatsCount());
        Assertions.assertEquals(4656, seats.getNonOccupiedSeatsCount());
    }

    @Test
    public void see_8_occupied_seat_with_sample_2() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-sample-2.txt");
        Assertions.assertEquals('L', seats.getStatus(3, 4));
        Assertions.assertEquals(8, seats.getSeenOccupiedSeatsCount(3, 4));
    }

    @Test
    public void see_no_occupied_seat_with_sample_3() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-sample-3.txt");
        Assertions.assertEquals('L', seats.getStatus(1, 1));
        Assertions.assertEquals(0, seats.getSeenOccupiedSeatsCount(1, 1));
    }

    @Test
    public void see_no_occupied_seat_with_sample_4() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-sample-4.txt");
        Assertions.assertEquals('L', seats.getStatus(3, 3));
        Assertions.assertEquals(0, seats.getSeenOccupiedSeatsCount(3, 3));
    }

    @Test
    public void stabilize_seen_with_37_occupied_seats() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-sample.txt");
        seats.setIsAttractive((x, y) -> seats.getSeenOccupiedSeatsCount(x, y) == 0);
        seats.setIsRepulsive((x, y) -> seats.getSeenOccupiedSeatsCount(x, y) >= 5);
        seats.step();
        Assertions.assertEquals('#', seats.getStatus(8, 0));
        Assertions.assertEquals(5, seats.getSeenOccupiedSeatsCount(8, 0));
        Assertions.assertEquals('#', seats.getStatus(0, 1));
        Assertions.assertEquals(4, seats.getSeenOccupiedSeatsCount(0, 1));
        seats.stabilize();
        Assertions.assertEquals(26, seats.getNextOccupiedSeatsCount());
        Assertions.assertEquals(45, seats.getNonOccupiedSeatsCount());
    }

    @Test
    public void stabilize_real_input_on_seen_seats() throws IOException, URISyntaxException {
        var seats = new SeatingSystem("day-11-input.txt");
        seats.setIsAttractive((x, y) -> seats.getSeenOccupiedSeatsCount(x, y) == 0);
        seats.setIsRepulsive((x, y) -> seats.getSeenOccupiedSeatsCount(x, y) >= 5);
        seats.stabilize();
        Assertions.assertEquals(1955, seats.getNextOccupiedSeatsCount());
        Assertions.assertEquals(4867, seats.getNonOccupiedSeatsCount());
    }
}
