package com.leroy.ronan.aoc2020.day2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class PasswordValidatorShould {

    @Test
    public void validate_first_sample() {
        var validator = new PasswordValidator();

        Assertions.assertTrue(validator.validate("1-3 a: abcde"));
    }

    @Test
    public void not_validate_second_sample() {
        var validator = new PasswordValidator();

        Assertions.assertFalse(validator.validate("1-3 b: cdefg"));
    }

    @Test
    public void validate_third_sample() {
        var validator = new PasswordValidator();

        Assertions.assertTrue(validator.validate("2-9 c: ccccccccc"));
    }

    @Test
    public void validate_samples() {
        var validator = new PasswordValidator();

        Assertions.assertEquals(2, validator.validate(
                List.of("1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc")
        ));
    }

    @Test
    public void validate_input() throws IOException, URISyntaxException {
        var validator = new PasswordValidator("day-2-input.txt");

        Assertions.assertEquals(607, validator.validateInput());
    }

    @Test
    public void validate_samples_with_second_policy() {
        var validator = new PasswordValidator();
        validator.setPolicy(new SecondPolicy());

        Assertions.assertEquals(1, validator.validate(
                List.of("1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc")
        ));
    }

    @Test
    public void validate_input_with_second_policy() throws IOException, URISyntaxException {
        var validator = new PasswordValidator("day-2-input.txt");
        validator.setPolicy(new SecondPolicy());

        Assertions.assertEquals(321, validator.validateInput());
    }
}
