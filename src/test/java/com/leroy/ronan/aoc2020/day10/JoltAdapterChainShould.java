package com.leroy.ronan.aoc2020.day10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class JoltAdapterChainShould {

    @Test
    public void produce_35_as_product_of_differences_for_sample_1() throws IOException, URISyntaxException {
        var chain = new JoltAdapterChain("day-10-sample-1.txt");
        Assertions.assertEquals(7, chain.getAdaptersTypeCount(1));
        Assertions.assertEquals(0,  chain.getAdaptersTypeCount(2));
        Assertions.assertEquals(5,  chain.getAdaptersTypeCount(3));
        Assertions.assertEquals(35, chain.getAdapterTypesProduct());
        Assertions.assertEquals(8, chain.getArrangements());
    }

    @Test
    public void produce_220_as_product_of_differences_for_sample_1() throws IOException, URISyntaxException {
        var chain = new JoltAdapterChain("day-10-sample-2.txt");
        Assertions.assertEquals(22, chain.getAdaptersTypeCount(1));
        Assertions.assertEquals(0, chain.getAdaptersTypeCount(2));
        Assertions.assertEquals(10,  chain.getAdaptersTypeCount(3));
        Assertions.assertEquals(220, chain.getAdapterTypesProduct());
        Assertions.assertEquals(19208, chain.getArrangements());
    }

    @Test
    public void produce_XXX_as_product_of_differences_for_input() throws IOException, URISyntaxException {
        var chain = new JoltAdapterChain("day-10-input.txt");
        Assertions.assertEquals(71, chain.getAdaptersTypeCount(1));
        Assertions.assertEquals(0, chain.getAdaptersTypeCount(2));
        Assertions.assertEquals(29,  chain.getAdaptersTypeCount(3));
        Assertions.assertEquals(2059, chain.getAdapterTypesProduct());
        Assertions.assertEquals(86812553324672L, chain.getArrangements());
    }

    @Test
    public void RENAME() {
        Assertions.assertEquals(1, new JoltAdapterChain(0, 3).getArrangements());
        Assertions.assertEquals(2, new JoltAdapterChain(0, 1, 3).getArrangements());
        Assertions.assertEquals(2, new JoltAdapterChain(0, 2, 3).getArrangements());
        Assertions.assertEquals(1, new JoltAdapterChain(0, 1, 4).getArrangements());
        Assertions.assertEquals(1, new JoltAdapterChain(0, 2, 4).getArrangements());
        Assertions.assertEquals(1, new JoltAdapterChain(0, 3, 4).getArrangements());
        Assertions.assertEquals(0, new JoltAdapterChain(0, 1, 5).getArrangements());
        Assertions.assertEquals(1, new JoltAdapterChain(0, 2, 5).getArrangements());
        Assertions.assertEquals(1, new JoltAdapterChain(0, 3, 5).getArrangements());
        Assertions.assertEquals(0, new JoltAdapterChain(0, 4, 5).getArrangements());
        Assertions.assertEquals(4, new JoltAdapterChain(0, 1, 2, 3).getArrangements());
        Assertions.assertEquals(3, new JoltAdapterChain(0, 1, 2, 4).getArrangements());
        Assertions.assertEquals(2, new JoltAdapterChain(0, 1, 2, 5).getArrangements());
        Assertions.assertEquals(0, new JoltAdapterChain(0, 1, 2, 6).getArrangements());
        Assertions.assertEquals(7, new JoltAdapterChain(0, 1, 2, 3, 4).getArrangements());
        Assertions.assertEquals(2, new JoltAdapterChain(0, 1, 4, 5, 6).getArrangements());
        Assertions.assertEquals(1, new JoltAdapterChain(12, 15, 16, 19, 22).getArrangements());
        Assertions.assertEquals(2, new JoltAdapterChain(6, 7, 10, 11, 12).getArrangements());
        Assertions.assertEquals(4, new JoltAdapterChain(4, 5, 6, 7).getArrangements());
        Assertions.assertEquals(2, new JoltAdapterChain(10, 11, 12).getArrangements());
        Assertions.assertEquals(1, new JoltAdapterChain(15, 16, 19, 22).getArrangements());
        Assertions.assertEquals(1, new JoltAdapterChain(0).getArrangements());
        Assertions.assertEquals(1, new JoltAdapterChain(0, 1).getArrangements());
        Assertions.assertEquals(2, new JoltAdapterChain(0, 1, 2).getArrangements());
        Assertions.assertEquals(4, new JoltAdapterChain(0, 1, 2, 3).getArrangements());
        Assertions.assertEquals(7, new JoltAdapterChain(0, 1, 2, 3, 4).getArrangements());
        Assertions.assertEquals(9, new JoltAdapterChain(0, 1, 2, 3, 4, 5, 8).getArrangements());
    }
}
