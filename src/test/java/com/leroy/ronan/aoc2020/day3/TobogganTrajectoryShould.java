package com.leroy.ronan.aoc2020.day3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class TobogganTrajectoryShould {

    @Test
    public void meet_7_trees_with_sample() throws IOException, URISyntaxException {
        var trajectory = new TobogganTrajectory("day-3-sample.txt");
        Assertions.assertEquals('.', trajectory.get(0, 0));
        Assertions.assertEquals('.', trajectory.get(3, 1));
        Assertions.assertEquals('#', trajectory.get(6, 2));
        Assertions.assertEquals(2, trajectory.countTrees(1, 1));
        Assertions.assertEquals(7, trajectory.countTrees(3, 1));
        Assertions.assertEquals(3, trajectory.countTrees(5, 1));
        Assertions.assertEquals(4, trajectory.countTrees(7, 1));
        Assertions.assertEquals(2, trajectory.countTrees(1, 2));
        Assertions.assertEquals(336, trajectory.response());
    }

    @Test
    public void meet_7_trees_with_input() throws IOException, URISyntaxException {
        var trajectory = new TobogganTrajectory("day-3-input.txt");
        Assertions.assertEquals('.', trajectory.get(0, 0));
        Assertions.assertEquals('#', trajectory.get(3, 1));
        Assertions.assertEquals('#', trajectory.get(6, 2));
        Assertions.assertEquals(93, trajectory.countTrees(1, 1));
        Assertions.assertEquals(164, trajectory.countTrees(3, 1));
        Assertions.assertEquals(82, trajectory.countTrees(5, 1));
        Assertions.assertEquals(91, trajectory.countTrees(7, 1));
        Assertions.assertEquals(44, trajectory.countTrees(1, 2));
        Assertions.assertEquals(5007658656L, trajectory.response());
    }
}
