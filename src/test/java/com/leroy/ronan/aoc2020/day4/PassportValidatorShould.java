package com.leroy.ronan.aoc2020.day4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class PassportValidatorShould {

    @Test
    public void validate_2_passports_from_sample() throws IOException, URISyntaxException {
        var validator = new PassportValidator("day-4-sample.txt");

        Assertions.assertEquals(4, validator.getNorthPoleCredentials().size());
        Assertions.assertEquals("860033327", validator.getNorthPoleCredentials().get(0).get("pid"));
        Assertions.assertTrue(validator.getNorthPoleCredentials().get(0).hasNeededFields());
        Assertions.assertFalse(validator.getNorthPoleCredentials().get(1).hasNeededFields());
        Assertions.assertEquals(2L, validator.count(NorthPoleCredentials::hasNeededFields));
    }

    @Test
    public void validate_0_passports_from_sample_invalid() throws IOException, URISyntaxException {
        var validator = new PassportValidator("day-4-sample-invalid.txt");

        Assertions.assertEquals(4, validator.getNorthPoleCredentials().size());
        Assertions.assertEquals(4L, validator.count(NorthPoleCredentials::hasNeededFields));
        Assertions.assertEquals(0L, validator.count(NorthPoleCredentials::isValid));
    }

    @Test
    public void validate_4_passports_from_sample_invalid() throws IOException, URISyntaxException {
        var validator = new PassportValidator("day-4-sample-valid.txt");

        Assertions.assertEquals(4, validator.getNorthPoleCredentials().size());
        Assertions.assertEquals(4L, validator.count(NorthPoleCredentials::hasNeededFields));
        Assertions.assertEquals(4L, validator.count(NorthPoleCredentials::isValid));
    }

    @Test
    public void validate_passports_from_input() throws IOException, URISyntaxException {
        var validator = new PassportValidator("day-4-input.txt");

        Assertions.assertEquals(245L, validator.count(NorthPoleCredentials::hasNeededFields));
        Assertions.assertEquals(133L, validator.count(NorthPoleCredentials::isValid));
    }
}
