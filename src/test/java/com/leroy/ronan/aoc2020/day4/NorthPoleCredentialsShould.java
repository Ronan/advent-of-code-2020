package com.leroy.ronan.aoc2020.day4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;

public class NorthPoleCredentialsShould {

    private NorthPoleCredentials credentials = new NorthPoleCredentials(Collections.emptyList());

    @Test
    public void validate_inputs() {
        Assertions.assertTrue(credentials.isValidByr("2002"));
        Assertions.assertFalse(credentials.isValidByr("2003"));

        Assertions.assertTrue(credentials.isValidHgt("60in"));
        Assertions.assertTrue(credentials.isValidHgt("190cm"));
        Assertions.assertFalse(credentials.isValidHgt("190in"));
        Assertions.assertFalse(credentials.isValidHgt("190"));

        Assertions.assertTrue(credentials.isValidHcl("#123abc"));
        Assertions.assertTrue(credentials.isValidHcl("#0aeec7"));
        Assertions.assertFalse(credentials.isValidHcl("#123abz"));
        Assertions.assertFalse(credentials.isValidHcl("123abc"));

        Assertions.assertTrue(credentials.isValidEcl("brn"));
        Assertions.assertFalse(credentials.isValidEcl("wat"));

        Assertions.assertTrue(credentials.isValidPid("000000001"));
        Assertions.assertFalse(credentials.isValidPid("0123456789"));
    }
}
