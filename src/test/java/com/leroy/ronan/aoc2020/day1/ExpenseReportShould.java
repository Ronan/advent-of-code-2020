package com.leroy.ronan.aoc2020.day1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class ExpenseReportShould {

    public static final List<Long> SAMPLE = List.of(
            1721L,
            979L,
            366L,
            299L,
            675L,
            1456L
    );

    @Test
    public void add_up_for_sample() {
        var report = new ExpenseReport(SAMPLE);
        var hash = report.addUp();

        Assertions.assertEquals(514579, hash);
    }

    @Test
    public void add_up_for_input() throws URISyntaxException, IOException {
        var report = new ExpenseReport("day-1-input.txt");
        var hash = report.addUp();

        Assertions.assertEquals(997899, hash);
    }

    @Test
    public void add_up_3_for_sample() {
        var report = new ExpenseReport(SAMPLE);
        var hash = report.addUp3();

        Assertions.assertEquals(241861950, hash);
    }

    @Test
    public void add_up_3_for_input() throws IOException, URISyntaxException {
        var report = new ExpenseReport("day-1-input.txt");
        var hash = report.addUp3();

        Assertions.assertEquals(131248694, hash);
    }
}
