package com.leroy.ronan.aoc2020.day16;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class TicketScannerShould {

    @Test
    public void provide_an_error_rate_of_71_for_sample_input() throws IOException, URISyntaxException {
        var scanner = new TicketScanner("day-16-sample.txt");
        Assertions.assertEquals(3, scanner.getRules().size());
        Assertions.assertEquals(new Ticket(7L,1L,14L), scanner.getMyTicket());
        Assertions.assertEquals(4, scanner.getNearbyTickets().size());
        Assertions.assertEquals(71L, scanner.getErrorRate());
    }

    @Test
    public void provide_an_error_rate_of_29878_for_real_input() throws IOException, URISyntaxException {
        var scanner = new TicketScanner("day-16-input.txt");
        Assertions.assertEquals(29878L, scanner.getErrorRate());
    }

    @Test
    public void find_the_ticket_fields_for_sample2_input() throws IOException, URISyntaxException {
        var scanner = new TicketScanner("day-16-sample-2.txt");
        Assertions.assertEquals(0L, scanner.getErrorRate());
        Assertions.assertEquals(12, scanner.getMyTicket("class"));
        Assertions.assertEquals(11, scanner.getMyTicket("row"));
        Assertions.assertEquals(13, scanner.getMyTicket("seat"));
    }

    @Test
    public void find_the_ticket_fields_for_input() throws IOException, URISyntaxException {
        var scanner = new TicketScanner("day-16-input.txt");
        Assertions.assertEquals(71, scanner.getMyTicket("class"));
        Assertions.assertEquals(855438643439L, scanner.getProductOf("departure"));
    }

}
