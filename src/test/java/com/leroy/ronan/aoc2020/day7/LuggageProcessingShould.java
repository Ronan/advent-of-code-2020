package com.leroy.ronan.aoc2020.day7;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Set;

public class LuggageProcessingShould {

    @Test
    public void handle_sample_rules() throws IOException, URISyntaxException {
        var processing = new LuggageProcessing("day-7-sample.txt");
        var rules = processing.getRules();
        Assertions.assertEquals(9, rules.size());
        Assertions.assertEquals(
                Set.of("bright white", "muted yellow"),
                processing.canContainDirectly("shiny gold")
        );
        Assertions.assertEquals(Collections.emptySet(), processing.getContainableIn("faded blue"));
        Assertions.assertEquals(Collections.emptySet(), processing.getContainableIn("dotted black"));
        Assertions.assertEquals(Set.of("faded blue", "dotted black"), processing.getContainableIn("vibrant plum"));
        Assertions.assertEquals(
                Set.of("faded blue", "dark olive", "dotted black", "vibrant plum"),
                processing.getContainableIn("shiny gold")
        );
        Assertions.assertEquals(
                Set.of("bright white", "muted yellow", "dark orange", "light red"),
                processing.canContain("shiny gold")
        );
        Assertions.assertEquals(
                4,
                processing.canContain("shiny gold").size()
        );
        Assertions.assertEquals(
                32,
                processing.shouldContainCount("shiny gold") -1
        );

    }
    @Test
    public void handle_input_rules() throws IOException, URISyntaxException {
        var processing = new LuggageProcessing("day-7-input.txt");
        var rules = processing.getRules();
        Assertions.assertEquals(594, rules.size());
        Assertions.assertEquals(
                Set.of("mirrored plum", "muted violet", "shiny fuchsia", "dark green", "dotted violet", "dull gray", "vibrant lime", "dark aqua"),
                processing.canContainDirectly("shiny gold")
        );
        Assertions.assertEquals(Collections.emptySet(), processing.getContainableIn("striped tomato"));
        Assertions.assertEquals(
                112,
                processing.canContain("shiny gold").size()
        );
        Assertions.assertEquals(
                6260,
                processing.shouldContainCount("shiny gold") -1
        );
    }
}
