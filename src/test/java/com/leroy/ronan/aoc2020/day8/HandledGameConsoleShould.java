package com.leroy.ronan.aoc2020.day8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static com.leroy.ronan.aoc2020.day8.ConsoleTermination.LOOP;
import static com.leroy.ronan.aoc2020.day8.ConsoleTermination.NORMAL;

public class HandledGameConsoleShould {

    @Test
    public void accumulate_5_using_sample_input() throws IOException, URISyntaxException {
        var console = new HandledGameConsole("day-8-sample.txt");
        var termination = console.run();
        Assertions.assertEquals(LOOP, termination);
        Assertions.assertEquals(5, console.getAccumulator());
    }

    @Test
    public void accumulate_using_day_input() throws IOException, URISyntaxException {
        var console = new HandledGameConsole("day-8-input.txt");
        var termination = console.run();
        Assertions.assertEquals(LOOP, termination);
        Assertions.assertEquals(1179, console.getAccumulator());
    }

    @Test
    public void terminate_with_8_using_fixed_sample_input() throws IOException, URISyntaxException{
        var console = new HandledGameConsole("day-8-sample.txt");
        console.fix(7);
        var termination = console.run();
        Assertions.assertEquals(NORMAL, termination);
        Assertions.assertEquals(8, console.getAccumulator());
    }

    @Test
    public void fix_sample_and_terminate() throws IOException, URISyntaxException {
        var console = new HandledGameConsole("day-8-sample.txt");
        Assertions.assertEquals(8, console.fix());
    }

    @Test
    public void fix_input_and_terminate() throws IOException, URISyntaxException {
        var console = new HandledGameConsole("day-8-input.txt");
        Assertions.assertEquals(1089, console.fix());
    }
}
