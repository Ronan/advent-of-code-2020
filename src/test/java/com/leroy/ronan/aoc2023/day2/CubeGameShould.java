package com.leroy.ronan.aoc2023.day2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static com.leroy.ronan.aoc2023.day2.CubeGame.game;
import static com.leroy.ronan.aoc2023.day2.CubeGame.hand;
import static java.util.Set.of;

public class CubeGameShould {

    @Test
    public void have_a_sum_of_8_for_sample() {
        var game = new CubeGame(
                """
                Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
                Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
                Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
                Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
                Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
                """);

        var sum = game.sum("12 red, 13 green, 14 blue");
        Assertions.assertEquals(8, sum);

        Assertions.assertEquals(hand().red(4).green(2).blue(6), game.getById(1).minimize());
        Assertions.assertEquals(hand().red(1).green(3).blue(4), game.getById(2).minimize());

        Assertions.assertEquals(2286, game.power());
    }

    @Test
    public void have_a_sum_of_1_for_sample_line_1() {
        var game = new CubeGame(
                """
                Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
                """);

        Assertions.assertEquals(
                of(game(1, hand().blue(3).red(4), hand().red(1).green(2).blue(6), hand().green(2))),
                game.games
        );

        var sum = game.sum("12 red, 13 green, 14 blue");

        Assertions.assertEquals(1, sum);
        Assertions.assertEquals(48, game.power());
    }

    @Test
    public void have_a_sum_of_1_for_game_input() throws URISyntaxException, IOException {
        var game = new CubeGame(
                Paths.get(getClass().getClassLoader().getResource("2023-day2-input.txt").toURI())
        );

        var sum = game.sum("12 red, 13 green, 14 blue");

        Assertions.assertEquals(2101, sum);
        Assertions.assertEquals(58269, game.power());
    }
}
