package com.leroy.ronan.aoc2023.day3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class EngineSchematicShould {

    @Test
    public void sum_sample_as_4361() {
        var schematic = new EngineSchematic(
                """
                467..114..
                ...*......
                ..35..633.
                ......#...
                617*......
                .....+.58.
                ..592.....
                ......755.
                ...$.*....
                .664.598..
                """.lines());

        Assertions.assertFalse(schematic.shouldBeAdded(114, 9, 0));
        Assertions.assertEquals(4361, schematic.sum());
        Assertions.assertEquals(467, schematic.partAt(1, 0));
        Assertions.assertEquals(467835, schematic.gearRatio());
    }

    @Test
    public void sum_input_as_527144() throws URISyntaxException, IOException {
        var schematic = new EngineSchematic(Files.lines(
                Paths.get(getClass().getClassLoader().getResource("2023-day3-input.txt").toURI())
        ));
        Assertions.assertEquals(527144, schematic.sum());
        Assertions.assertEquals(81463996, schematic.gearRatio());
    }
}
