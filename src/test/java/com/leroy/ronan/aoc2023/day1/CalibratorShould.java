package com.leroy.ronan.aoc2023.day1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class CalibratorShould {

    @Test
    public void calibrate_sample_into_142() {
        var calibrator = new Calibrator(
                List.of(
                        "1abc2",
                        "pqr3stu8vwx",
                        "a1b2c3d4e5f",
                        "treb7uchet"
                )
        );

        var calibration = calibrator.calibrate();

        Assertions.assertEquals(142, calibration);
    }

    @Test
    public void calibrate_small_sample_into_12() {
        var calibrator = new Calibrator(
                List.of(
                        "1abc2"
                )
        );

        var calibration = calibrator.calibrate();

        Assertions.assertEquals(12, calibration);
    }

    @Test
    public void calibrate_complex_sample_into_281() {
        var calibrator = new Calibrator(
                List.of(
                        "two1nine",
                        "eightwothree",
                        "abcone2threexyz",
                        "xtwone3four",
                        "4nineeightseven2",
                        "zoneight234",
                        "7pqrstsixteen"
                )
        );
        var calibration = calibrator.calibrate(true);
        Assertions.assertEquals(281, calibration);
    }

    @Test
    public void calibrate_eightwothree_sample_into_83() {
        var calibrator = new Calibrator(
                List.of(
                        "eightwothree"
                )
        );
        var calibration = calibrator.calibrate(true);
        Assertions.assertEquals(83, calibration);
    }

    @Test
    public void calibrate_abcone2threexyz_sample_into_13() {
        var calibrator = new Calibrator(
                List.of(
                        "abcone2threexyz"
                )
        );
        var calibration = calibrator.calibrate(true);
        Assertions.assertEquals(13, calibration);
    }

    @Test
    public void calibrate_input() throws IOException, URISyntaxException {
        var calibrator = new Calibrator("2023-day1-input.txt");

        var calibration = calibrator.calibrate();

        Assertions.assertEquals(55607, calibration);
    }

    @Test
    public void calibrate_complex_input() throws IOException, URISyntaxException {
        var calibrator = new Calibrator("2023-day1-input.txt");

        var calibration = calibrator.calibrate(true);

        Assertions.assertEquals(55291, calibration);
    }
}
