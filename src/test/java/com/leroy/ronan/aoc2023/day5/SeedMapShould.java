package com.leroy.ronan.aoc2023.day5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SeedMapShould {

    @Test
    public void map_seed_to_location_with_sample() {
        var map = new SeedMap("""
                seeds: 79 14 55 13
                
                seed-to-soil map:
                50 98 2
                52 50 48
                
                soil-to-fertilizer map:
                0 15 37
                37 52 2
                39 0 15
                
                fertilizer-to-water map:
                49 53 8
                0 11 42
                42 0 7
                57 7 4
                
                water-to-light map:
                88 18 7
                18 25 70
                
                light-to-temperature map:
                45 77 23
                81 45 19
                68 64 13
                
                temperature-to-humidity map:
                0 69 1
                1 0 69
                
                humidity-to-location map:
                60 56 37
                56 93 4
                """.lines());
        Assertions.assertEquals(35, map.getLowestLocationNumber());
        Assertions.assertEquals(46, map.getLowestLocationNumberExt());
    }

    @Test
    public void map_seed_to_location_with_input() throws URISyntaxException, IOException {
        var map = new SeedMap(Files.lines(
                Paths.get(getClass().getClassLoader().getResource("2023-day5-input.txt").toURI())
        ));
        Assertions.assertEquals(318728750, map.getLowestLocationNumber());
    }

    @Test
    public void map_seed_to_location_with_extended_input() throws URISyntaxException, IOException {
        var map = new SeedMap(Files.lines(
                Paths.get(getClass().getClassLoader().getResource("2023-day5-input.txt").toURI())
        ));
        Assertions.assertEquals(37384986, map.getLowestLocationNumberExt());
    }
}
