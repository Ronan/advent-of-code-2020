package com.leroy.ronan.aoc2023.day6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BoatRacesShould {

    @Test
    public void compute_sample_as_288() {
        var races = new BoatRaces("""
                Time:      7  15   30
                Distance:  9  40  200
                """.lines());
        Assertions.assertEquals(288, races.compute());
        Assertions.assertEquals(71503, races.computeBig());
    }

    @Test
    public void compute_input_as_1660968() {
        var races = new BoatRaces("""
                Time:        47     98     66     98
                Distance:   400   1213   1011   1540
                """.lines());
        Assertions.assertEquals(1660968, races.compute());
        Assertions.assertEquals(26499773, races.computeBig());
    }

    @Test
    public void find_the_margins() {
        Assertions.assertEquals(4, new BoatRaces.BoatRace(7, 9).margin());
        Assertions.assertEquals(8, new BoatRaces.BoatRace(15, 40).margin());
        Assertions.assertEquals(9, new BoatRaces.BoatRace(30, 200).margin());
    }
}
