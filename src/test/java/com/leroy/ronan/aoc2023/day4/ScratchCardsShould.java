package com.leroy.ronan.aoc2023.day4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ScratchCardsShould {

    @Test
    public void analyze_sample() {
        var cards = new ScratchCards("""
                Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
                Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
                Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
                Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
                Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
                Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
                """.lines());

        Assertions.assertEquals(13, cards.sum());
        Assertions.assertEquals(30, cards.count());
    }

    @Test
    public void parse_sample() {
        var card = ScratchCards.ScratchCard.parse("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53");
        Assertions.assertEquals(1, card.id());
        Assertions.assertEquals(5, card.wins().size());
        Assertions.assertEquals(8, card.owned().size());
    }

    @Test
    public void analyze_input() throws URISyntaxException, IOException {
        var cards = new ScratchCards(Files.lines(
                Paths.get(getClass().getClassLoader().getResource("2023-day4-input.txt").toURI())
        ));
        Assertions.assertEquals(28750, cards.sum());
        Assertions.assertEquals(10212704, cards.count());
    }
}
